# Frontend homework: Hangman Game

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



Your task is to build a simple hangman game. Here’s how it should work:


Pick a random word from the attached list, and present the user with a number of empty squares, corresponding to the letters of the word.

Allow the user to enter a letter (any way you like: window prompt, input field, keypress event, virtual keyboard, handwriting recognition, etc.)

If the letter is included in the word, reveal every occurrence of it in the squares. If the word is complete, the user wins.

If not, we have attached a simple SVG drawing of a hangman. It should start out blank, and with each missed attempt you should reveal one more line (i.e. one more child element in the SVG). If the drawing is complete, the user loses.


To get full points, your app should also fulfil all of the below requirements:

It should be responsive.

It should have a landing page for the app that explains the rules, and a separate screen for the actual game.

We expect an SPA.

Allow the user to play more than one game without reloading the page.

Allow the user to set the length of the word before a new game (determine the minimum and maximum length from the list of words).

Present the user with a score counter that increments after every incorrect guess.

Allow the user to continue the previously played game after reloading the page.


We have attached a basic UI design scheme; implement it as closely as possible. Aside from that, use whatever framework you wish, or none at all. This is your project. Just make it happen.


You need to deliver a working game that we can play through until the end otherwise we can’t evaluate your submission. It doesn’t have to be perfect but it cannot contain game breaking bugs.


We’ll be expecting your submission via a GitLab link. Please commit regularly, at least after each feature while you work.
