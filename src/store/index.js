import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate';

const store = createStore({
	plugins: [createPersistedState()],
	state: {
		word: '',
		usedCharacters: [],
		mistakes: 0,
		score: 0
	},
	mutations: {
		setWord(state, word) {
			state.word = word;			
		},
		setUsedCharacters(state, usedCharacters) {
			state.usedCharacters = usedCharacters;
		},
		setMistakes(state, mistakes) {
			state.mistakes = mistakes;
		},
		setScore(state, score) {
			state.score = score;
		}
	}
});

export default store;